# Getting started with Anki
Hi, if you are wondering how to start using Anki with _Kartenaale_ content,
then this page is for you.

If you are completely new to Anki, see the user guide in [English](GUIDE.md) or
[German](ANLEITUNG.md).

If you already know Anki and just want to download APKGs, check out the release
sections in the various packs in
[kartenaale/packs](https://gitlab.phaidra.org/kartenaale/packs), e.g.
[Sinologie Anki Decks STEOP](https://gitlab.phaidra.org/kartenaale/packs/sinology-1/-/releases)
for STEOP.

If you want to contribute your own Anki content, either contribute to an
existing pack with a merge request or check out the starter project
in
[kartenaale/templates/anki-pack](https://gitlab.phaidra.org/kartenaale/templates/anki-pack)
to start your own new pack.
