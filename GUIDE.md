# Kartenaal Guide to Anki (English | [Deutsch](ANLEITUNG.md))

## What is Anki and what is the Kartenaal group?

Go straight to the [Quickstart Guide](#quickstart-guide) if you want to get started quickly.

[Anki](https://apps.ankiweb.net/) is an application for digital flashcards that helps you learn by keeping track of your progress and showing you precisely the cards that are still giving you trouble. It works on mobile, PC, Mac, and over the web.

_Kartenaale_ is a group at the University of Vienna that shares decks for and knowledge about the popular _Anki_ app. Its decks hosted on the university Gitlab can be helpful for your studies at the University of Vienna. You can use them to learn _vocabulary_ or _facts_.

The various [packs](https://gitlab.phaidra.org/kartenaale/packs) on Giothub include, taken together:

* Vocabulary, radicals, character components, simplified and traditional included:
    * _Chinesisch lernen für die Sinologie (Band 1)_
    * _Chinesisch lernen für die Sinologie (Band 2)_
*   Facts:
    * Introduction to Chinese Cultural History
    * Chinesische Politische Geschichte
    * Politik und Recht Chinas
    * Chinese Cultural Spaces

These kinds of cards are included:

* Q/A (facts),
* listening to and understanding Chinese speech,
* reading Hànzì and Pīnyīn,
* writing Hànzì,
* translating German words and expressions to Chinese,
* identify radicals.

Some features:
* stroke order animations,
* touch a character to see stroke order details incl numbering,
* add your own notes and automatically get radical and listening exercises in addition to classic vocabulary cards.

Among other places, the Kartenaale group meets on [Whatsapp](https://chat.whatsapp.com/JFKpfmq29yM2xKcSu7JQib).

## Quickstart Guide

If you just want to practice, follow these steps.

### Step 1: Download apkg

Content for Anki can be imported from Anki Packages (APKG files). For kartenaale content, go to the release page of an APKG pack in
[kartenaale/Anki Packs](https://gitlab.phaidra.org/kartenaale/packs), e.g.:
* [Sinologie Anki Decks STEOP](https://gitlab.phaidra.org/kartenaale/packs/sinology-1/-/releases) for language and facts needed for the STEOP exam,
* [Sinologie Anki Decks 2](https://gitlab.phaidra.org/kartenaale/packs/sinology-2/-/releases) for content shortly after STEOP,
* [Sinologie Anki Decks Extra](https://gitlab.phaidra.org/kartenaale/packs/sinology-extra/-/releases) for bonus sinology content.

The list of APKG is available through a link in the description of the newest release. Following that link, select and download the `.apkg` file you are interested in. In the end you should have a file like `Chinesisch lernen für die Sinologie-2.0.0.apkg` on your machine.

### Step 2: Install Anki

If you don't yet have Anki installed, download a version for your PC from the official [Anki](https://apps.ankiweb.net/#download) website. Install it to your PC.

Alternatively, install [AnkiDroid](https://play.google.com/store/apps/details?id=com.ichi2.anki&hl=de_AT&gl=US&pli=1) (for Android phones, free) or [Anki Mobile](https://apps.apple.com/de/app/ankimobile-flashcards/id373493387) (mobile Apple devices, e.g. iPhone or iPad, ~30€).

### Step 3: Import the Deck

Import the _apkg_ deck into Anki with _File | Import…_ on Anki Desktop:

![_File | Import…_ dialog in Anki Desktop](screenshots/screenshot-anki-import.png)

![_sinologie-anki-pack-1.1.9.zip_ in the file open dialog of Anki Desktop](screenshots/screenshot-anki-import-dialog.png)

You can also download the APKG onto your phone and then add it to AnkiDroid. Use "Import" on the top right for this:

![APKG in AnkiDroid 1](screenshots/phone-import-1.jpg)

![APKG in AnkiDroid 2](screenshots/phone-import-2.jpg)

![APKG in AnkiDroid 3](screenshots/phone-import-3.jpg)

That's all you need to practice. Go ahead and try it out!

### Step 4: Sync with Anki Web (optional)

A nice feature of Anki is that you can also practice on your phone or other device. If you don't need this right now, skip this step completely or do it later.

First, make sure that your collection of cards is synced to the web. Hit the _Sync_ button on the top right in Anki Desktop to do this. You may need to register if you don't yet have an account on Anki Web.

When the synchronization has finished, you can practice over the web on any device including your mobile phone by visiting [https://ankiweb.net/](https://ankiweb.net/) in your browser. If you prefer a native app, try [AnkiDroid](https://play.google.com/store/apps/details?id=com.ichi2.anki&hl=de_AT&gl=US&pli=1) (for Android phones, free) or [Anki Mobile](https://apps.apple.com/de/app/ankimobile-flashcards/id373493387) (mobile Apple devices, e.g. iPhone or iPad, ~30€).

### Step 5: Configuring text-to-speech on AnkiDroid (optional)

When using AnkiDroid, it is recommended to activate text-to-speech under _Settings | Advanced | Text to speech_. This will speak the characters out loud when practicing listening (or for other cards when the backside is shown). To replay the sound, use the play button in the menu bar. This button may be invisible for you unless you change it to _Always show_ under _Settings | Reviewing | App bar buttons | Replay audio_.

On other platforms like _AnkiWeb_, _Anki Mobile_ or on the desktop version of Anki, no configuration is needed for text-to-speech. You can use the play button within the template to replay without extra configuration.

### Step 5: Enjoy!

That's it, have fun learning!

## Advanced usage

### How can I add more vocabulary in Anki
If you want to add your own cards, please add the
[APKG for custom content](https://gitlab.phaidra.org/kartenaale/packs/your-custom-vocabulary/-/releases).
and then use the type _Vocab simple_ under _Add_ in Anki.

It has the same features as e.g. the HSK decks, but you have to fill in less
fields.

### How do I view the individual strokes of a Chinese character?

Whenever you see a chinese character, there is also an animation to showcase the correct stroke order. If the animation is too fast or too slow for you, instead tap or click the character in question. This will show you all the strokes in sequence, e.g.: ![Screenshot of the template in AnkiDroid](screenshots/screenshot-practice-writing.png)

### Updating to a newer version

If a newer version of this template is available and you want to update, repeat
the guide above with the newer apkg. Your progress with the old deck will not
be affected.

### Advanced usage and customization

Apart from adding your own content, you can customize many other things directly in Anki. For example, you can add extra CSS for minor optical tweaks like changing the font size.

If you want to do some deeper customization or add new features, have a look at the [templates project](https://gitlab.phaidra.org/kartenaale/build/card-templates).

### Contributing

It's easy to extend these decks with your own content, or to make your own decks. If you're happy with what you made and want to share it, consider sharing it with the Kartenaale group. The fruits of your labor may end up here if you like.

Feel free to raise a GitLab issue if you are experiencing issues or have any suggestions or even pull requests to make this pack better.

If you just have some suggestions, please also add an issue on GitLab.

If you want to contribute new words but are not that technical, just open an issue and post the new words there as an Anki collection or some other format of your choice.