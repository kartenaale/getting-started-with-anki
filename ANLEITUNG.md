# Kartenaal Guide to Anki (Deutsch | [English](GUIDE.md))

## Was ist Anki und was ist die Kartenaal-Gruppe?

Du kannst auch direkt mit der [Schnellstartanleitung](#schnellstartanleitung) beginnen.

[Anki](https://apps.ankiweb.net/) ist ein Karteikartenprogramm, das deine Fortschritte über längere Zeit speichert und damit genau das abprüft, was noch schwer fällt. Es funktioniert mobil, am PC, auf Mac, oder im Web.

_Kartenaale_ ist eine Gruppe an der Universität Wien, die Anki Decks und Wissen dazu teilt. Auf dem Gitlab der Uni gibt es verschiedene Decks für _Anki_, die dir im Sinologiestudium an der Uni Wien helfen können. Du kannst damit _Vokabeln_ und _Fakten_ lernen.

Die verschiedenen [Packs](https://gitlab.phaidra.org/kartenaale/packs) auf Gitlab enthalten zusammen:

*   Vokabeln, Radikale, Zeichenkomponenten, mit Langzeichen und Kurzzeichen:
    * _Chinesisch lernen für die Sinologie (Band 1)_
    * _Chinesisch lernen für die Sinologie (Band 2)_
*   Fakten:
    * Introduction to Chinese Cultural History
    * Chinesische Politische Geschichte
    * Politik und Recht Chinas
    * Chinese Cultural Spaces

Es gibt folgende Arten von Karten:

* Frage/Antwort (Fakten),
* Gesprochenes Chinesisch verstehen,
* Hànzì und Pīnyīn lesen,
* Hànzì schreiben,
* deutsche Begriffe auf Chinesisch übersetzen,
* Radikale erkennen.

Einige Features:

* Strichfolge-Animationen,
* Berühre ein Zeichen für Strichfolge-Details inkl. Nummerierung,
* Lege eigene Karten an und bekomme Radikal- und Hörverständniskarten automatisch zu den klassischen Vokabelkarten.

Kartenaale treffen sicher unter anderem auf [Whatsapp](https://chat.whatsapp.com/JFKpfmq29yM2xKcSu7JQib).

## Schnellstartanleitung

Um einfach nur zu üben, folge diesem Abschnitt.

### Schritt 1: apkg herunterladen

Inhalte für Anki kann man von Anki Packages (APKG-Dateien) importieren.
Für Kartenaale-Inhalte, besuche die Release-Seite von einem der APKG-Packs in [kartenaale/Anki Packs](https://gitlab.phaidra.org/kartenaale/packs), z.B.:
* [Sinologie Anki Decks STEOP](https://gitlab.phaidra.org/kartenaale/packs/sinology-1/-/releases) enthält Sprachcontent und Fakten für alle STEOP-relevanten LVA,
* [Sinologie Anki Decks 2](https://gitlab.phaidra.org/kartenaale/packs/sinology-2/-/releases) ist kurz nach der STEOP interessant,
* [Sinologie Anki Decks Extra](https://gitlab.phaidra.org/kartenaale/packs/sinology-extra/-/releases) ist interessantes Bonuswissen für Sinolog:innen.

Die Liste an APKGs ist jeweils beim neuesten Release in der Beschreibung verlinkt. Auf dieser Seite können die `.apkg`-Dateien für die du dich  interessierst heruntergeladen werden. Am Ende solltest du eine Datei wie
`Chinesisch lernen für die Sinologie-2.0.0.apkg` auf deinem Gerät haben.

### Schritt 2: Anki intallieren

Wenn du Anki noch nicht auf deinem PC installiert hast, lade es von der offiziellen [Anki](https://apps.ankiweb.net/#download)\-Seite herunter und installiere.

Alternativ geht auch [AnkiDroid](https://play.google.com/store/apps/details?id=com.ichi2.anki&hl=de_AT&gl=US&pli=1) (for Android-Handys, gratis) oder [Anki Mobile](https://apps.apple.com/de/app/ankimobile-flashcards/id373493387) (mobile Apple-Geräte, z.B. iPhone or iPad, ~30€).

### Schritt 3: apkg importieren

Importiere das _apkg_\-Deck in Anki, z.B. am Desktop unter _File | Import…_:

![_File | Import…_ dialog in Anki Desktop](screenshots/screenshot-anki-import.png)

![_sinologie-anki-pack-1.1.9.zip_ in the file open dialog of Anki Desktop](screenshots/screenshot-anki-import-dialog.png)

Alternativ kannst das APKG auch auf dein Smartphone laden und dort in AnkiDroid hinzufügen. Verwende dazu "Import" oben rechts:

![APKG in AnkiDroid 1](screenshots/phone-import-1.jpg)

![APKG in AnkiDroid 2](screenshots/phone-import-2.jpg)

![APKG in AnkiDroid 3](screenshots/phone-import-3.jpg)

Damit ist alles erledigt um zu lernen. Probiere es aus!

### Schritt 4: Synchronisieren mit Anki Web (optional)

Schön an Anki ist, dass man auch am Smartphone oder anderen Geräten lernen kann. Du kannst diesen Schritt überspringen falls du das (noch) nicht brauchst, oder du kannst es später erledigen.

Stelle zuerst sicher, dass deine Kartensammlung mit Anki Web synchron ist. Betätige dazu den _Sync_\-Knopf open rechts. Du musst dich registrieren, falls noch nicht geschehen.

Wenn die Synchronisierung abgeschlossen ist, kannst du über einen Web-Browser auf deinem Smartphone unter [https://ankiweb.net/](https://ankiweb.net/) lernen. Wenn du eine App bevorzugst, probiere [AnkiDroid](https://play.google.com/store/apps/details?id=com.ichi2.anki&hl=de_AT&gl=US&pli=1) (for Android-Handys, gratis) oder [Anki Mobile](https://apps.apple.com/de/app/ankimobile-flashcards/id373493387) (mobile Apple-Geräte, z.B. iPhone or iPad, ~30€).

### Schritt 5: Vorlese-Funktion in AnkiDroid konfigurieren (optional)

Bei Benutzung von AnkiDroid wird empfohlen Text-to-speech unter _Settings | Advanced | Text to speech_ zu aktivieren. Wie bei der Desktop-App werden dann bei den Zuhör-Übungen chinesische Begriffe laut abgespielt. Für andere Karten-Typen erfolgt die Sprachausgabe auf der Rückseite der Karteikarte. Um erneut abzuspielen, gibt es in _AnkiDroid_ einen Play-Button in der Menüleiste oben. Der Button ist möglicherweise ausgeblendet. Er lässt sich unter _Settings | Reviewing | App bar buttons | Replay audio_ einblenden, indem man die Einstellung auf _Always show_ setzt.

Auf anderen Plattformen wie _AnkiWeb_, _Anki Mobile_ oder auf der PC-Version von Anki ist keine Konfiguration erforderlich. Hier kann der Play-Button auf der Karte verwendet werden.

### Step 5: Genießen!

Das war's, viel Spaß beim lernen!

## Fortgeschrittene Anwendung

### Wie kann ich mehr Vokabeln in Anki hinzufügen?
Importiere das
[APKG für eigenen Content](https://gitlab.phaidra.org/kartenaale/packs/your-custom-vocabulary/-/releases).
und unter _Add_ in Anki wähle als Typ _Vocab simple_.

Du bekommst damit die gleichen Features wie etwa die HSK-Decks, musst aber
nicht so viele Felder ausfüllen.

### Wie kann ich mir die Strichfolge für ein chinesisches Zeichen ansehen?

Wenn du ein animiertes chinesisches Zeichen siehst, kannst du darauf klicken bzw. es berühren. Es öffnet sich eine Tabelle mit den einzelnen Strichen, z.B.: ![Screenshot of the template in AnkiDroid](screenshots/screenshot-practice-writing.png)

### Auf eine neuere Version updaten

Wenn eine neuere Version als apkg verfügbar ist und du updaten möchtest, folge noch einmal den Schritten oben. Dein bisheriger Fortschritt bleibt auf jeden
Fall erhalten.

### Fortgeschrittene Anwendung und Anpassung

Außer deine eigenen Inhalte hinzuzufügen, kannst du auch viele andere Dinge direkt in Anki für dich anpassen. Zum Beispiel können kleinere optische Veränderungen wie eine Änderung der Schriftgröße mit CSS gemacht werden. Für tiefergehende Anpassungen oder neue Features, siehe auch das README im [templates repository](https://gitlab.phaidra.org/kartenaale/build/card-templates).

## Zum Kartenpack beitragen

Du kannst die Decks auch leicht mit deinen eigenen Vokabeln oder Fakten erweitern oder eigene Decks anlegen. Wenn du mit dem Ergebnis zufrieden bist, überlege dir ob du es mit anderen Kartenaalen teilen möchtest. Die Früchte deiner Arbeit könnten letzten Endes hier landen, wenn du das möchtest.

Du kannst gern ein GitLab-Issue anlegen wenn du Schwierigkeiten bei der Verwendung hast. Wenn du selbst Features hinzufügen möchtest, lege ein Issue an oder mache direkt einen Pull-Request auf

Für Verbesserungsvorschläge auch gerne Issues anlegen.

Wenn du selbst kein sehr technischer Benutzer bist aber trotzdem neue Wörter hinzufügen willst, kannst du auch gerne ein Issue mit deinen Wörtern anlegen und ich helfe dabei.